INCLUDES = -I/usr/include/libusb-1.0
LDLIBS = -L/usr/local/lib/ -lusb-1.0

CFLAGS ?= -g -Wall -Werror -Wshadow -O2 -pipe -fno-strict-aliasing ${INCLUDES} ${LIBS}

%.o: %.c
	$(CC) $(CFLAGS) -o $@ -c $<

usb_serial: usb_serial.o 

clean: 
	-rm -f *.o usb_serial
