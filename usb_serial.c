/*
 * libusb example program to list devices on the bus
 * Copyright (C) 2007 Daniel Drake <dsd@gentoo.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>

#include <libusb-1.0/libusb.h>

static int print_devs(libusb_device **devs, int bus, int port)
{
	libusb_device *dev;
	libusb_device_handle *devHandle;
	unsigned char strSerial[256];
	int r;
	int rc = -1;

	int i = 0;

	while ((dev = devs[i++]) != NULL) {
		struct libusb_device_descriptor desc;

		r = libusb_get_device_descriptor(dev, &desc);
		if (r != LIBUSB_SUCCESS)
			continue;

		if (desc.iSerialNumber == 0)
			continue;

		if (libusb_get_bus_number(dev) != bus || libusb_get_port_number(dev) != port) 
			continue;

		r = libusb_open (dev, &devHandle);
		if (r != LIBUSB_SUCCESS)
		      continue;

		r = libusb_get_string_descriptor_ascii(devHandle, desc.iSerialNumber, strSerial, 256);
		if (r < 0) {
			libusb_close(devHandle);
			continue;
		}

		libusb_close(devHandle);
		printf ("%s\n", strSerial);
		rc = 0;
	}
	
	return rc;
}

int main(int argc, char **argv)
{
	libusb_device **devs;
	int r;
	ssize_t cnt;
	
	if (argc < 3) {
		printf("Usage %s <bus> <port>\n",argv[0]);
		exit(1);
	}

	int bus = strtol(argv[1], NULL, 0);
	int port = strtol(argv[2], NULL, 0);

	r = libusb_init(NULL);
	if (r < 0)
		return r;

	cnt = libusb_get_device_list(NULL, &devs);
	if (cnt < 0)
		return (int) cnt;

	r = print_devs(devs, bus, port);
	libusb_free_device_list(devs, 1);

	libusb_exit(NULL);
	return r;
}

